package com.example.bellido.asteroides

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import android.content.SharedPreferences
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.widget.EditText
import android.widget.Toast
import android.content.pm.PackageManager
import android.os.Build
import android.os.Looper
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat


class MainActivity : AppCompatActivity(), OnClickListener {

    //lateinit var mp : MediaPlayer
    // private var almacen = AlmacenPUntuacionesFicheroInterno(this).getInstance()!!

    //VARIABLES NECESARIAS

    private var almacen: AlmacenPuntuaciones? = null
    var posicion: Int = 0
    lateinit var prefs: SharedPreferences
    var musicaOn: Boolean = false
    var nombreJugador: String? = null
    var puntos: Int = 0
    var tipoalmacen: String = ""

    private val PERMISOS_EXTERNO = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private val PETICION_INICIAL: Int = 123


    //METODOS

    fun getAlmacen(): AlmacenPuntuaciones? {
        return almacen
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var acercaDe: Button = button5
        //mp = MediaPlayer.create(this, R.raw.portada)
        button3.setOnClickListener(this)
        acercaDe.setOnClickListener(this)
        button4.setOnClickListener(this)
        button6.setOnClickListener(this)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)


    }


    //CICLO DE VIDA

    override fun onStart() {
        super.onStart()
        Log.d("CICLO DE VIDA", "Se ejecuta onStart()")
        musicaOn = prefs.getBoolean("musica", false)
        tipoalmacen = prefs.getString("tipo_almacen", "").toString()
        comprobarAlmacen()
        comprobarMusica()
    }

    override fun onResume() {
        super.onResume()
        Log.d("CICLO DE VIDA", "Se ejecuta onResume()")
        comprobarMusica()
    }

    override fun onPause() {
        super.onPause()
        Log.d("CICLO DE VIDA", "Se ejecuta onPause()")
        stopService(Intent(this, ServicioMusica::class.java))
        musicaOn = false
    }

    override fun onStop() {
        super.onStop()
        Log.d("CICLO DE VIDA", "Se ejecuta onStop()")
        stopService(Intent(this, ServicioMusica::class.java))
        musicaOn = false
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("CICLO DE VIDA", "Se ejecuta onRestart()")
        startService(Intent(this, ServicioMusica::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("CICLO DE VIDA", "Se ejecuta onDestroy()")
    }


    override fun onSaveInstanceState(guardarEstado: Bundle) {
        super.onSaveInstanceState(guardarEstado)
        //posicion = mp.currentPosition;
        //guardarEstado!!.putInt("pos", posicion)
    }


    override fun onRestoreInstanceState(cargarEstado: Bundle?) {
        super.onRestoreInstanceState(cargarEstado)
        //posicion = cargarEstado!!.getInt("pos")
        //mp.seekTo(posicion)
    }

    //COMPROBAR MUSICA
    fun comprobarMusica() {
        if (!musicaOn) {
            startService(Intent(this, ServicioMusica::class.java))
            musicaOn = true
        } else {
            stopService(Intent(this, ServicioMusica::class.java))
            musicaOn = false
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
//COMPROBAR PERMISOS
    fun comprobarPermisos(){
        if (checkSelfPermission(PERMISOS_EXTERNO[0]) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, PERMISOS_EXTERNO, PETICION_INICIAL)
        }
    }

    @SuppressLint("NewApi")
//COMPROBAR EL TIPO DE ALMACENAMIENTO DE PUNTUACIONES
    fun comprobarAlmacen() {
        if (tipoalmacen == "Array") {
            almacen = AlmacenPuntuacionesArray().getInstance()!!
        } else if (tipoalmacen == "Archivo de preferencias") {
            almacen = AlmacenPuntuacionesPreferencias(this).getInstance()!!
        } else if (tipoalmacen == "Fichero Interno") {
            almacen = AlmacenPUntuacionesFicheroInterno(this).getInstance()!!
        } else if (tipoalmacen == "Fichero Externo"){
            comprobarPermisos()
                almacen = AlmacenPUntuacionesFicheroExterno(this).getInstance()!!
        } else if(tipoalmacen == "Fichero APL"){
                comprobarPermisos()
                almacen = AlmacenPUntuacionesFicheroExternoApl(this).getInstance()!!
        }else if(tipoalmacen == "Recurso RAW (sólo lectura)"){
            almacen = AlmacenPUntuacionesFicheroRAW(this).getInstance()!!
        } else if(tipoalmacen == "Recurso ASSETS (sólo lectura)"){
        almacen = AlmacenPUntuacionesFicheroAssets(this).getInstance()!!
        } else if(tipoalmacen == "Archivo XML procesado con SAX"){
            almacen = AlmacenPuntuacionesSAX(this).getInstance()!!
        } else if(tipoalmacen == "Archivo XML procesado con DOM"){
            almacen = AlmacenPuntuacionesDOM(this).getInstance()!!
        }
        else if(tipoalmacen == "Base de datos SQLite (tablas relacionadas)"){
            almacen = AlmacenPuntuacionesSQLiteRelacional(this).getInstance()!!
        }else if(tipoalmacen == "Servicio WEB REST (JSON)"){
            almacen = AlmacenPuntuacionesJSON(this).getInstance()!!
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1234 && resultCode == Activity.RESULT_OK && data != null) {
            puntos = data.extras!!.getInt("puntuacion")
            val dialogo = AlertDialog.Builder(this)
            dialogo.setTitle("Introduce tu nombre")
            val jugador = EditText(this)
            dialogo.setView(jugador)
            dialogo.setPositiveButton("Aceptar", DialogInterface.OnClickListener { dialog, which ->
                nombreJugador = jugador.text.toString()
                almacen!!.guardarPuntuacion(puntos, nombreJugador!!, System.currentTimeMillis())
                lanzarPuntuaciones()
            })
            dialogo.create().show()
        }
    }


    private fun lanzarJuego() {
        val i = Intent(this, Juego::class.java)
        startActivityForResult(i, 1234)
    }

    override fun onClick(v: View?) {
        when (v) {

            button3 -> lanzarJuego()


            button4 -> {
                var i: Intent = Intent(this, PrefsActivity::class.java)
                startActivity(i)
            }

            button5 -> lanzarAcercaDe()


            button6 -> lanzarPuntuaciones()


        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        // menuInflater.inflate(R.menu.menu_main, menu)
        menuInflater.inflate(R.menu.bar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.acercaDe -> lanzarAcercaDe()
            R.id.config -> {
                var i: Intent = Intent(this, PrefsActivity::class.java)
                startActivity(i)
            }

        }
        return true
    }

    private fun lanzarAcercaDe() {
        var i: Intent = Intent(this, AcercaDe::class.java)
        startActivity(i)
    }

    private fun lanzarPuntuaciones() {
        var i: Intent = Intent(this, Puntuaciones3::class.java)
        startActivity(i)
    }

}
