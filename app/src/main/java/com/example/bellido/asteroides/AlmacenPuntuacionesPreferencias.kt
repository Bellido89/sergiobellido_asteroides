package com.example.bellido.asteroides

import android.content.Context
import android.R.id.edit
import android.content.SharedPreferences
/**
 * Created by Bellido on 12/02/2018.
 */
class AlmacenPuntuacionesPreferencias() : AlmacenPuntuaciones {


    private val PREFERENCIAS = "puntuaciones"
    private lateinit var context: Context
    private var hayPuntos = false

    constructor(context: Context) : this(){
        this.context = context
    }

    companion object {
        private var instancia: AlmacenPuntuacionesPreferencias? = null
        private set
    }


    fun getInstance(): AlmacenPuntuacionesPreferencias? {
        if(instancia == null) {
            instancia = AlmacenPuntuacionesPreferencias(context)
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {
        val preferencias = context.getSharedPreferences(PREFERENCIAS, Context.MODE_PRIVATE)
        val editor = preferencias.edit()
        var ultimaPuntuacion: String
        var contador: Int = 0
        while (contador <= 9) {

            ultimaPuntuacion = preferencias.getString("puntuacion" + (contador+1), "")
            if (ultimaPuntuacion != "")
                editor.putString("puntuacion" + contador, ultimaPuntuacion)
            else if(contador == 9)
                editor.putString("puntuacion" + contador, puntos.toString() + " " + nombre)

            editor.apply()
            contador++
        }
    }

    override fun listaPuntuaciones(cantidad: Int): ArrayList<String> {
        val result = ArrayList<String>()
        val preferencias = context.getSharedPreferences(PREFERENCIAS, Context.MODE_PRIVATE)
        var puntuacion: String
        var contador = 0
        while (contador <= 9) {
            puntuacion = preferencias.getString("puntuacion" + contador, "")
            if (puntuacion != "")
                result.add(puntuacion)
            contador++
        }
        return result
    }
    override fun hayPuntuaciones(): Boolean {
        return hayPuntos
    }
}