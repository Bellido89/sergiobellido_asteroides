package com.example.bellido.asteroides

import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import android.widget.Toast


/**
 * Created by Bellido on 18/02/2018.
 */
class AlmacenPuntuacionesSQLite(context: Context)  : SQLiteOpenHelper(context,"puntuaciones", null, 1), AlmacenPuntuaciones  {

    var context : Context = context


    override fun onCreate(db:SQLiteDatabase) {
        db.execSQL("CREATE TABLE puntuaciones (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "puntos INTEGER, nombre TEXT, fecha LONG)")
    }

    companion object {
        private var instancia: AlmacenPuntuacionesSQLite? = null
            private set
    }

    fun getInstance(): AlmacenPuntuacionesSQLite? {
        if(instancia == null) {
            synchronized(AlmacenPuntuacionesSQLite::class.java) {
                instancia = AlmacenPuntuacionesSQLite(context)
            }
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {
        val db = writableDatabase
        db.execSQL("INSERT INTO puntuaciones VALUES ( null, " +
                puntos + ", '" + nombre + "', " + fecha + ")")
        db.close()
    }

    override fun listaPuntuaciones(cantidad: Int): ArrayList<String> {
        val result = ArrayList<String>()
        val db = readableDatabase
        val CAMPOS = arrayOf("puntos", "nombre")
        val cursor = db.query("puntuaciones", CAMPOS, null, null, null, null, "puntos DESC", Integer.toString(cantidad))
        while (cursor.moveToNext()) {
            result.add(cursor.getInt(0).toString() + " " + cursor.getString(1))
        }
        cursor.close()
        db.close()
        return result
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // En caso de una nueva versión habría que actualizar las tablas
    }

    override fun hayPuntuaciones(): Boolean {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT COUNT(*) FROM puntuaciones", null)
        cursor.moveToFirst()
        return cursor.getInt(0) > 0
    }
}