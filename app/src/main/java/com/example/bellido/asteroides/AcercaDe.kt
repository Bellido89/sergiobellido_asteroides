package com.example.bellido.asteroides

import android.app.Activity
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import kotlinx.android.synthetic.main.acercade.*


class AcercaDe : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.acercade)
        var tv : TextView  = TextoAcercaDe
        var texto : String = getString(R.string.acercaDe);
        tv.setText(Html.fromHtml(texto));

    }
}
