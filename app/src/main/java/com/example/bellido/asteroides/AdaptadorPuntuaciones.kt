/*
package com.example.bellido.asteroides

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView


class AdaptadorPuntuaciones() : BaseAdapter() {

    override fun getItem(position: Int): Any {
        return lista.elementAt(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return lista.size
    }


    private lateinit var activity: Activity
    private lateinit var lista: ArrayList<String>

    constructor(activity: Activity, lista: ArrayList<String>) : this() {
        this.activity = activity
        this.lista = lista

    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolder
        var view: View? = convertView

        //SI LA VISTA ESTÁ VACIA INFLAMOS
        if (view == null) {
            var inflater: LayoutInflater = activity.layoutInflater
            view = inflater.inflate(R.layout.elemento_lista, null, true)
            holder = ViewHolder()
            holder.titulo = view.findViewById(R.id.titulo)
            holder.icono = view.findViewById(R.id.icono)
            view.setTag(holder)

            //EN CASO CONTRARIO AÑADIMOS AL HOLDER EL TAG CORRESPONDIENTE
        } else {
            holder = view.getTag() as ViewHolder
        }
            holder.titulo.setText(lista.elementAt(position))
            when ((Math.round(Math.random() * 3)).toInt()) {
                0 -> holder.icono.setImageResource(R.drawable.asteroide1)
                1 -> holder.icono.setImageResource(R.drawable.asteroide2)
                else -> holder.icono.setImageResource(R.drawable.asteroide3)
            }
            return view
    }

    //CLASE VIEWHOLDER PARA OPITMIZACION DE LA LISTVIEW
    internal inner class ViewHolder {
        lateinit var titulo : TextView
        lateinit var icono : ImageView
    }
}



*/
