package com.example.bellido.asteroides

import android.content.Context
import android.util.Log
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Bellido on 16/02/2018.
 */
class AlmacenPuntuacionesSAX : AlmacenPuntuaciones {


    var FICHERO = "puntuaciones.xml"
    var context: Context
    var lista: ListaPuntuacionesSAX
    private var listaCargada: Boolean = false

     constructor(context: Context) {
        this.context = context
        lista = ListaPuntuacionesSAX()
        listaCargada = false
    }

    companion object {
        private var instancia: AlmacenPuntuacionesSAX? = null
            private set
    }

    fun getInstance(): AlmacenPuntuacionesSAX? {
        if(instancia == null) {
            synchronized(AlmacenPuntuacionesSAX::class.java) {
                instancia = AlmacenPuntuacionesSAX(context)
            }
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {
        try {
            if (!listaCargada) {
                lista.leerXML(context.openFileInput(FICHERO))
                listaCargada = true
            }
        } catch (e: FileNotFoundException) {
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

        lista.nuevo(puntos, nombre, fecha)
        try {
            lista.escribirXML(context.openFileOutput(FICHERO,
                    Context.MODE_PRIVATE))
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

    }


    override fun listaPuntuaciones(cantidad:Int): ArrayList<String> {
        try
        {
            if (!listaCargada)
            {
                lista.leerXML(context.openFileInput(FICHERO))
                listaCargada = true
            }
        }
        catch (e:Exception) {
            Log.e("Asteroides", e.message, e)
        }
        return lista.aVectorString()
    }

    override fun hayPuntuaciones(): Boolean {
        val puntuaciones = listaPuntuaciones(10)
        // Sólo lo invocamos para saber si hay puntuaciones
        return !puntuaciones.isEmpty()
    }
}