package com.example.bellido.asteroides

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.os.Environment
import android.util.Log
import android.widget.Toast
import java.io.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Bellido on 14/02/2018.
 */
class AlmacenPUntuacionesFicheroExternoApl() : AlmacenPuntuaciones {

    lateinit var context : Context
    lateinit var FICHERO : String
    lateinit var  ruta : File
    private var estadoSD : String = Environment.getExternalStorageState()

    constructor(context: Context) : this() {
        this.context = context
        ruta = context.getExternalFilesDir(null)
        FICHERO = ruta.absolutePath + "/puntosAPL.txt"
        if (!ruta.exists()){
            ruta.mkdir()
        }
    }


    companion object {
        private var instancia: AlmacenPUntuacionesFicheroExternoApl? = null
            private set
    }

    fun getInstance(): AlmacenPUntuacionesFicheroExternoApl? {
        if(instancia == null) {
            instancia = AlmacenPUntuacionesFicheroExternoApl(context)
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {

        try {
            if (estadoSD == (Environment.MEDIA_MOUNTED)) {
                val f = FileOutputStream(FICHERO, true)
                val texto = puntos.toString() + " " + nombre + "\n"
                f.write(texto.toByteArray())
                f.close()
            } else {
                Toast.makeText(context, "No se ha encontrado almacenamiento externo", Toast.LENGTH_SHORT).show()
            }
        }   catch (e: Exception) {
                Log.e("Asteroides", e.message, e)
            }
    }

    override fun listaPuntuaciones(cantidado: Int): ArrayList<String> {

            val result = ArrayList<String>()
            try {
                if (estadoSD ==  (Environment.MEDIA_MOUNTED)  || estadoSD ==  (Environment.MEDIA_MOUNTED_READ_ONLY)) {
                    val f = FileInputStream(FICHERO)
                val entrada = BufferedReader(InputStreamReader(f))
                var n = 0
                var linea: String?
                do {
                    linea = entrada.readLine()
                    if (linea != null) {
                        result.add(linea)
                        n++
                    }
                } while (n < cantidado && linea != null)
                f.close()

            } else {
                    Toast.makeText(context, "No se ha encontrado almacenamiento externo", Toast.LENGTH_SHORT).show()
                }
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }
        return result
    }

    override fun hayPuntuaciones(): Boolean {
        val puntuaciones = listaPuntuaciones(10)
        return !puntuaciones.isEmpty()
    }
}