package com.example.bellido.asteroides

import android.content.Context
import android.util.Log
import org.w3c.dom.Document
import java.io.FileNotFoundException
import java.io.InputStream
import javax.xml.parsers.DocumentBuilderFactory
import java.io.OutputStream
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult


/**
 * Created by Bellido on 18/02/2018.
 */
class AlmacenPuntuacionesDOM : AlmacenPuntuaciones {

     var FICHERO = "puntuacionesDOM.xml"
     var context: Context? = null
     var document: Document? = null
     var documentoCargado: Boolean = false

    constructor(context: Context){
        this.context = context
    }

    companion object {
       var instancia : AlmacenPuntuacionesDOM? = null
            private set
    }

    fun getInstance(): AlmacenPuntuacionesDOM? {
        if(instancia == null) {
            synchronized(AlmacenPuntuacionesDOM::class.java) {
                instancia = AlmacenPuntuacionesDOM(context!!)
            }
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos:Int, nombre:String,
                                   fecha:Long) {
        try
        {
            if (!documentoCargado)
            {
                leerXML(context!!.openFileInput(FICHERO))
            }
        }
        catch (e: FileNotFoundException) {
            crearDocumento()
        }
        catch (e:Exception) {
            Log.e("Asteroides", e.message, e)
        }
        nuevo(puntos, nombre, fecha)
        try
        {
            escribirXML(context!!.openFileOutput(FICHERO,
                    Context.MODE_PRIVATE))
        }
        catch (e:Exception) {
            Log.e("Asteroides", e.message, e)
        }
    }

    override fun listaPuntuaciones(cantidad: Int): ArrayList<String> {
        try {
            if (!documentoCargado) {
                leerXML(context!!.openFileInput(FICHERO))
            }
        } catch (e: FileNotFoundException) {
            crearDocumento()
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

        return aVectorString()
    }

    override fun hayPuntuaciones(): Boolean {
        val puntuaciones = listaPuntuaciones(10)
        // Sólo lo invocamos para saber si hay puntuaciones
        return !puntuaciones.isEmpty()
    }


    private fun crearDocumento() {
        try {
            var factory = DocumentBuilderFactory.newInstance()
            var builder = factory.newDocumentBuilder()
            document = builder.newDocument()
            val raiz = document!!.createElement("lista_puntuaciones")
            document!!.appendChild(raiz)
            documentoCargado = true
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

    }


    @Throws(Exception::class)
    private fun leerXML(entrada: InputStream) {
        var factory = DocumentBuilderFactory.newInstance()
        var builder = factory.newDocumentBuilder()
        document = builder.parse(entrada)
        documentoCargado = true
    }

    private fun nuevo(puntos: Int, nombre: String, fecha: Long) {
        var ePuntuacion = document!!.createElement("puntuacion")
        ePuntuacion.setAttribute("fecha", fecha.toString())
        var eNombre = document!!.createElement("nombre")
        var texto = document!!.createTextNode(nombre)
        eNombre.appendChild(texto)
        ePuntuacion.appendChild(eNombre)
        var ePuntos = document!!.createElement("puntos")
        texto = document!!.createTextNode(puntos.toString())
        ePuntos.appendChild(texto)
        ePuntuacion.appendChild(ePuntos)
        var raiz = document!!.getDocumentElement()
        raiz.appendChild(ePuntuacion)
    }

    private fun aVectorString(): ArrayList<String> {
        var result = ArrayList<String>()
        var nombre = ""
        var puntos = ""
        var raiz = document!!.getDocumentElement()
        var puntuaciones = raiz.getElementsByTagName("puntuacion")
        for (i in 0 until puntuaciones.length) {
            val puntuacion = puntuaciones.item(i)
            val propiedades = puntuacion.childNodes
            for (j in 0 until propiedades.length) {
                var propiedad = propiedades.item(j)
                var etiqueta = propiedad.nodeName
                if (etiqueta == "nombre") {
                    nombre = propiedad.firstChild.nodeValue
                } else if (etiqueta == "puntos") {
                    puntos = propiedad.firstChild.nodeValue
                }
            }
            result.add(nombre + " " + puntos)
        }
        return result
    }

    @Throws(Exception::class)
    private fun escribirXML(salida: OutputStream) {
        val factory = TransformerFactory.newInstance()
        val transformer = factory.newTransformer()
        transformer.setOutputProperty(OutputKeys
                .OMIT_XML_DECLARATION, "yes")
        transformer.setOutputProperty(OutputKeys.INDENT, "yes")
        val source = DOMSource(document)
        val result = StreamResult(salida)
        transformer.transform(source, result)
    }

}