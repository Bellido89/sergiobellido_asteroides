package com.example.bellido.asteroides

import android.os.Bundle
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment

/**
 * Created by belli on 25/10/2017.
 */

class PrefsFragment : PreferenceFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var category : String = arguments.getString("category")
        if (category != null){
            if (category == "categoria_general"){
                addPreferencesFromResource(R.xml.prefs_general)
            } else if (category == "categoria_multijugador"){
                addPreferencesFromResource(R.xml.prefs_multijugador)
            } else if (category == "categoria_almacen"){
                addPreferencesFromResource(R.xml.almacen)
            }
        }
    }
}
