package com.example.bellido.asteroides

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View

/**
 * Created by Bellido on 11/12/2017.
 */
open class Grafico {

    var drawable: Drawable? = null //Imagen que dibujaremos
    var posX: Double = 0.0 //Posición
    var posY: Double = 0.0 //Posición
    var incX: Double = 0.0 //Velocidad desplazamiento
    var incY: Double = 0.0 //Velocidad desplazamiento
    var angulo: Int = 0 //Ángulo
    var rotacion: Int = 0 //Velocidad rotación
    var ancho: Int = 0 //Dimensiones de la imagen
    var alto: Int = 0 //Dimensiones de la imagen
    var radioColision: Int = 0 //Para determinar colisión
    //Donde dibujamos el gráfico (usada en view.invalidate)
    lateinit var view: View
    // Para determinar el espacio a borrar (view.invalidate)
    val MAX_VELOCIDAD: Int = 20

    constructor()

    constructor(view: View, drawable: Drawable) {
        this.view = view
        this.drawable = drawable
        ancho = drawable.intrinsicWidth
        alto = drawable.intrinsicHeight
        radioColision = (alto+ancho)/4
    }

    fun dibujaGrafico(canvas: Canvas){
        canvas.save()
        var x: Int = (posX + ancho/2).toInt()
        var y: Int = (posY + ancho/2).toInt()
        canvas.rotate(angulo.toFloat(), x.toFloat(), y.toFloat())
        drawable!!.setBounds(posX.toInt(), posY.toInt(), posX.toInt() + ancho, posY.toInt() + alto)
        drawable!!.draw(canvas)
        canvas.restore()
        var rInval: Int = Math.hypot(ancho.toDouble(),alto.toDouble()).toInt()/2 + MAX_VELOCIDAD
        view.invalidate(x - rInval, y - rInval, x + rInval, y + rInval)
    }

    fun incrementaPos(factor: Double){
        posX += incX * factor
        // Si salimos de la pantalla, corregimos posición
        if(posX < -ancho/2) { posX = view.width.toDouble() - ancho/2;}
        if(posX > view.getWidth() - ancho/2) {posX = -ancho.toDouble()/2;}
        posY += incY * factor;
        if(posY < -alto/2) {posY = view.height.toDouble() -alto/2;}
        if(posY > view.getHeight() -alto/2) {posY = -alto.toDouble()/2;}
        angulo += rotacion * factor.toInt(); //Actualizamos ángulo
    }

    fun distancia(g: Grafico): Double {
        return Math.hypot(posX - g.posX, posY - g.posY)
    }

    fun verificaColision(g: Grafico): Boolean {
        return(distancia(g) < (radioColision + g.radioColision))
    }
}
