package com.example.bellido.asteroides

import kotlin.collections.ArrayList


interface AlmacenPuntuaciones {
    fun guardarPuntuacion(puntos:Int, nombre:String, fecha:Long)
    fun listaPuntuaciones(cantidado : Int): ArrayList<String>
    fun hayPuntuaciones() : Boolean
}
