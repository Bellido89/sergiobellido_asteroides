package com.example.bellido.asteroides

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Bellido on 14/02/2018.
 */
class AlmacenPUntuacionesFicheroInterno(private var context: Context) : AlmacenPuntuaciones {

    private val FICHERO : String = "puntuaciones.txt"

    companion object {
        private var instancia: AlmacenPUntuacionesFicheroInterno? = null
            private set
    }

    fun getInstance(): AlmacenPUntuacionesFicheroInterno? {
        if(instancia == null) {
            instancia = AlmacenPUntuacionesFicheroInterno(context)
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {

        try {
            val f = context.openFileOutput(FICHERO, Context.MODE_APPEND)
            val texto = puntos.toString() + " " + nombre + "\n"
            f.write(texto.toByteArray())
            f.close()
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }
    }

    override fun listaPuntuaciones(cantidado: Int): ArrayList<String> {

        val result = ArrayList<String>()
        try {
            val f = context.openFileInput(FICHERO)
            val entrada = BufferedReader(InputStreamReader(f))
            var n = 0
            var linea: String?
            do {
                linea = entrada.readLine()
                if (linea != null) {
                    result.add(linea)
                    n++
                }
            } while (n < cantidado && linea != null)
            f.close()
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

        return result
    }

    override fun hayPuntuaciones(): Boolean {
        val puntuaciones = listaPuntuaciones(10)
        return !puntuaciones.isEmpty()
    }
}