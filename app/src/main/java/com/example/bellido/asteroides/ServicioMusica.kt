package com.example.bellido.asteroides

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.widget.Toast

/**
 * Created by Bellido on 02/02/2018.
 */
class ServicioMusica : Service() {


    lateinit var notificationManager: NotificationManager
    var ID_NOTIFICACION = 1

    internal lateinit var reproductor: MediaPlayer
    override fun onCreate() {
        reproductor = MediaPlayer.create(this, R.raw.portada)

        notificationManager= getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    }


    override fun onStartCommand(intent: Intent, flags:Int, idArranque:Int):Int {


        reproductor.start()
        var intent = Intent(this, MainActivity::class.java)
        var pendingIntent = PendingIntent
                .getActivity(this, 0, intent, 0)
        var notification = Notification.Builder(this)
                .setContentTitle("Asteroides")
                .setContentText("Musica de Fondo")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .getNotification()

        notification.flags = notification.flags or Notification.FLAG_SHOW_LIGHTS
        notificationManager.notify(ID_NOTIFICACION, notification)
        return START_STICKY

    }

    override fun onDestroy() {
        reproductor.stop()
        notificationManager.cancel(ID_NOTIFICACION);
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}