package com.example.bellido.asteroides

import org.xml.sax.Attributes
import org.xml.sax.SAXException
import org.xml.sax.helpers.DefaultHandler

/**
 * Created by Bellido on 16/02/2018.
 */
class ManejadorXML : DefaultHandler() {


     lateinit var cadena:StringBuilder
     lateinit var puntuacion:Puntuacion
    private val listaPuntuaciones = ArrayList<Puntuacion>()


    fun getListaPuntuaciones():ArrayList<Puntuacion> {
        return listaPuntuaciones
    }


    @Throws(SAXException::class)
    override fun startDocument() {
        cadena = StringBuilder()
    }


    @Throws(SAXException::class)
    override fun startElement(uri:String, nombreLocal:String, nombreCualif:String, atr: Attributes) {

        cadena.setLength(0)
        if (nombreLocal == "puntuacion")
        {
            puntuacion = Puntuacion()
            puntuacion.fecha = (java.lang.Long.parseLong(atr
                    .getValue("fecha")))
        }
    }

    override fun characters(ch:CharArray, comienzo:Int, lon:Int) {
        cadena.append(ch, comienzo, lon)
    }

    @Throws(SAXException::class)
    override fun endElement(uri:String, nombreLocal:String, nombreCualif:String) {
        if (nombreLocal == "puntos")
        {
            puntuacion.puntos = (Integer.parseInt(cadena.toString()))
        }
        else if (nombreLocal == "nombre")
        {
            puntuacion.nombre = (cadena.toString())
        }
        else if (nombreLocal == "puntuacion")
        {
            listaPuntuaciones.add(puntuacion)
        }
    }

    @Throws(SAXException::class)
    override fun endDocument() {}
}