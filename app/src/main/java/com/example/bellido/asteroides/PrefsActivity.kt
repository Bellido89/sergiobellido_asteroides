package com.example.bellido.asteroides

import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.preference.EditTextPreference
import android.preference.ListPreference
import android.preference.PreferenceActivity
import android.support.v7.widget.DrawableUtils
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.support.v7.widget.Toolbar
import android.widget.Toast

/**
 * Created by belli on 31/10/2017.
 */

class PrefsActivity : PreferenceActivity(){
    lateinit var toolbar : Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepararLayout()

    }

    fun prepararLayout(){
        var root : ViewGroup = findViewById(android.R.id.content)

        var content : View = root.getChildAt(0)
        var toolbarContainer : LinearLayout = View.inflate(this, R.layout.activity_prefs, null) as LinearLayout

        root.removeAllViews()
        toolbarContainer.addView(content)
        root.addView(toolbarContainer)

        toolbar = toolbarContainer.findViewById(R.id.toolbar)
        toolbar.title = title

        var upArrow : Drawable = resources.getDrawable(R.drawable.ic_arrow_back)
        upArrow.setColorFilter(resources.getColor(R.color.toolbar_icon), PorterDuff.Mode.SRC_ATOP)

        toolbar.navigationIcon = upArrow
        toolbar.setNavigationOnClickListener { finish() }


    }



    override fun onBuildHeaders(target: MutableList<PreferenceActivity.Header>) {
        super.onBuildHeaders(target)
        loadHeadersFromResource(R.xml.prefs_principal, target)

    }

    override fun isValidFragment(fragmentName: String?): Boolean {
        return fragmentName.equals(PrefsFragment::class.java.name)
    }
}
