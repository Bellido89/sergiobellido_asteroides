package com.example.bellido.asteroides

import kotlin.collections.ArrayList


class AlmacenPuntuacionesArray : AlmacenPuntuaciones {

    private var puntuaciones : ArrayList<String> = ArrayList()

companion object {
        private var instancia : AlmacenPuntuacionesArray? = null
        private set
}

    override fun guardarPuntuacion(puntos:Int, nombre:String, fecha:Long) {
       puntuaciones.add(0, puntos.toString() + "" + nombre)

    }
    override fun listaPuntuaciones(cantidad: Int): ArrayList<String> {
        return puntuaciones
    }

    fun inicializarDatos() {
                var s : String
               for (i in 1..100){
                   s = (Math.round((Math.random()*1000).toFloat())).toString() + " Usuario " + i.toString()
                   puntuaciones.add(s)
               }
    }

    fun getInstance() : AlmacenPuntuaciones? {
        inicializarDatos()
        if(instancia == null) {
            instancia = AlmacenPuntuacionesArray()
        }
        return instancia
    }

    override fun hayPuntuaciones() : Boolean {
        return (!puntuaciones.isEmpty())
    }
}

