package com.example.bellido.asteroides

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.widget.Toast
import org.json.JSONObject
import android.util.Log
import org.apache.http.NameValuePair
import org.apache.http.message.BasicNameValuePair



class AlmacenPuntuacionesJSON(context: Context) : AlmacenPuntuaciones {

    var context : Context = context
    var jsonManager : JSONManager = JSONManager()
    val urlObtener : String = "http://proves.iesperemaria.com/asteroides/puntuaciones"
    val urlGrabar : String = "http://proves.iesperemaria.com/asteroides/puntuaciones/nueva/"
    private val urlTotalPuntuaciones = "http://proves.iesperemaria.com/asteroides/puntuaciones/total"
    var puntuaciones : ArrayList<String> = ArrayList()
    var finalizado : Boolean = false
    var contadorPuntuaciones = 0


    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instancia: AlmacenPuntuacionesJSON? = null
            private set
    }

    fun getInstance(): AlmacenPuntuacionesJSON? {
        if(instancia == null) {
            synchronized(AlmacenPuntuacionesJSON::class.java) {
                instancia = AlmacenPuntuacionesJSON(context)
            }
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {
        finalizado = false
        val nuevaPuntuacion = NuevaPuntuacion().execute(puntos.toString(), nombre, fecha.toString())
        try {
            nuevaPuntuacion.get() // esperar a que acabe la AsyncTask
        } catch (e: Exception) {
        }

    }

    override fun listaPuntuaciones(cantidado: Int): ArrayList<String> {
        val cant = cantidado
        finalizado = false
        val listar = PuntuacionJSON().execute(cant.toString())
        while (!finalizado && !listar.isCancelled()) {
            // esperamos a finalizar;
        }
//pDialog.dismiss();
        return puntuaciones
    }

    override fun hayPuntuaciones(): Boolean {
       // val puntuaciones : ArrayList<String> = listaPuntuaciones(1)
        val puntuaciones = ContadorPuntuaciones().execute()
        while (!finalizado && !puntuaciones.isCancelled) {
            // esperamos a finalizar;
        }
        return (contadorPuntuaciones > 0)
    }


     inner class PuntuacionJSON : AsyncTask<String, Void, Void>(){

         override fun doInBackground(vararg params: String?): Void? {

             try {
                 var parametros = ArrayList<NameValuePair>()
                 parametros.add(BasicNameValuePair("cantidad", params[0]))
                 var jsonString = jsonManager.getJsonString(urlObtener, "GET", parametros)
                 var jsonObject = JSONObject(jsonString)
                 var jsonArray = jsonObject.getJSONArray("puntuaciones")
                 for (i in 0 until jsonArray.length()) {
                     val nodo = jsonArray.getJSONObject(i)
                     val puntuacion = nodo.getString("puntos") + " " + nodo.getString("nombre")
                     puntuaciones.add(puntuacion)
                 }
                 finalizado = true
             } catch (e: Exception) {
                 e.printStackTrace()
                 Toast.makeText(context, "Error accediendo al servicioo", Toast.LENGTH_LONG).show()
             }

             return null
         }

         override fun onPostExecute(result: Void?) {
             super.onPostExecute(result)
             finalizado = true
         }
     }

    inner class NuevaPuntuacion : AsyncTask<String, Void, Boolean>(){



        override fun doInBackground(vararg params: String?): Boolean {

            val jsonString: String
            val jsonObject: JSONObject
            try {
                val parametros = ArrayList<NameValuePair>()
                parametros.add(BasicNameValuePair("puntos", params[0]))
                parametros.add(BasicNameValuePair("nombre", params[1]))
                parametros.add(BasicNameValuePair("fecha", params[2]))
                jsonString = jsonManager.getJsonString(urlGrabar, "POST", parametros)
                jsonObject = JSONObject(jsonString)
                val id = Integer.parseInt(jsonObject.getString("id"))
                if (id <= 0) {
                    Log.e("Asteroides", "Error: respuesta de servidor incorrecta")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(context, "Error accediendo al servicio", Toast.LENGTH_LONG).show()
            }
            return if (isCancelled) false
            else true
        }

        override fun onPostExecute(result: Boolean?) {
            super.onPostExecute(result)
            finalizado = result!!
        }

    }

    inner class ContadorPuntuaciones : AsyncTask<String, Void, Boolean>() {

        override fun doInBackground(vararg params: String?): Boolean? {
            val jsonString: String
            val jsonObject: JSONObject
            try {
                val parametros = ArrayList<NameValuePair>()
                jsonString = JSONManager().getJsonString(urlTotalPuntuaciones, "GET", null)
                jsonObject = JSONObject(jsonString)
                contadorPuntuaciones = Integer.parseInt(jsonObject.getString("total"))
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(context, "Error accediendo al servicio", Toast.LENGTH_LONG).show()
            }
            return !isCancelled
        }

        override fun onPostExecute(resultado: Boolean?) {
            finalizado = resultado!!
        }
    }

}