package com.example.bellido.asteroides

import android.os.Bundle
import android.preference.PreferenceActivity



class Prefs : PreferenceActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager
                .beginTransaction()
                .replace(android.R.id.content, PrefsFragment())
                .commit()
    }
}
