package com.example.bellido.asteroides

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.puntuaciones.*
import org.w3c.dom.Text

/**
 * Created by Bellido on 09/11/2017.
 */
class Puntuaciones3 : AppCompatActivity(){


    lateinit var recycler : RecyclerView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.puntuaciones)

        var almacen: AlmacenPuntuaciones? = null

        //Obtener almacen de puntuaciones
        var tipoSeleccionado = PreferenceManager.getDefaultSharedPreferences(this).getString("tipo_almacen", "")

        if (tipoSeleccionado == "Array") {
            almacen = AlmacenPuntuacionesArray().getInstance()
        } else if (tipoSeleccionado == "Archivo de preferencias") {
            almacen = AlmacenPuntuacionesPreferencias(this).getInstance()
        } else if (tipoSeleccionado == "Fichero Interno") {
            almacen = AlmacenPUntuacionesFicheroInterno(this).getInstance()
        } else if (tipoSeleccionado == "Fichero Externo") {
            almacen = AlmacenPUntuacionesFicheroExterno(this).getInstance()
        } else if (tipoSeleccionado == "Fichero APL") {
            almacen = AlmacenPUntuacionesFicheroExternoApl(this).getInstance()
        } else if (tipoSeleccionado == "Recurso RAW (sólo lectura)") {
            almacen = AlmacenPUntuacionesFicheroRAW(this).getInstance()!!
        } else if (tipoSeleccionado == "Recurso ASSETS (sólo lectura)") {
            almacen = AlmacenPUntuacionesFicheroAssets(this).getInstance()!!
        }else if(tipoSeleccionado == "Archivo XML procesado con SAX"){
            almacen = AlmacenPuntuacionesSAX(this).getInstance()!!
        }else if(tipoSeleccionado == "Archivo XML procesado con DOM"){
            almacen = AlmacenPuntuacionesDOM(this).getInstance()!!
        }else if(tipoSeleccionado == "Base de datos SQLite"){
            almacen = AlmacenPuntuacionesSQLite(this).getInstance()!!
        }else if(tipoSeleccionado == "Base de datos SQLite (tablas relacionadas)"){
            almacen = AlmacenPuntuacionesSQLiteRelacional(this).getInstance()!!
        }else if(tipoSeleccionado == "Servicio WEB REST (JSON)"){
            almacen = AlmacenPuntuacionesJSON(this).getInstance()!!
        }




        recycler = recycler_id
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        var adaptador = AdapterRecycler(almacen!!)
        recycler.adapter = adaptador


    }


}
