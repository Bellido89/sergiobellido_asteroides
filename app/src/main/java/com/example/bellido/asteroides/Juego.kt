package com.example.bellido.asteroides

import android.app.Activity
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast

/**
 * Created by Bellido on 11/12/2017.
 */
class Juego : Activity() {

    //VARIABLES JUEGOVIEW
    lateinit var juegoView : JuegoView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_juego)
        juegoView = findViewById(R.id.JuegoView)
        juegoView.setContenedor(this)


    }

    override fun onPause() {
        super.onPause()
        juegoView.thread.pausar()
        juegoView.desactivarSensores()
    }

    override fun onResume() {
        super.onResume()
        juegoView.thread.reanudar()
    }

    override fun onDestroy() {
        juegoView.thread.detener()
        juegoView.desactivarSensores()
        super.onDestroy()
    }


}