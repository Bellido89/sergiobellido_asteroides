package com.example.bellido.asteroides

import android.content.Context
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase



/**
 * Created by Bellido on 18/02/2018.
 */
class AlmacenPuntuacionesSQLiteRelacional (context: Context) : SQLiteOpenHelper(context,"puntuaciones", null, 2), AlmacenPuntuaciones {

    var context : Context = context

    companion object {
        private var instancia: AlmacenPuntuacionesSQLiteRelacional? = null
            private set
    }

    fun getInstance(): AlmacenPuntuacionesSQLiteRelacional? {
        if(instancia == null) {
            synchronized(AlmacenPuntuacionesSQLiteRelacional::class.java) {
                instancia = AlmacenPuntuacionesSQLiteRelacional(context)
            }
        }
        return instancia
    }


    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {
        val db = writableDatabase
        guardarPuntuacion(db, puntos, nombre, fecha)
    }

    fun guardarPuntuacion(db: SQLiteDatabase, puntos: Int,
                          nombre: String, fecha: Long) {
        val usuario = buscaInserta(db, nombre)
        db.execSQL("INSERT INTO puntuaciones2 VALUES ( null, " +
                puntos + ", " + fecha + ", " + usuario + ")")
    }

    private fun buscaInserta(db: SQLiteDatabase, nombre: String): Int {
        val cursor = db.rawQuery("SELECT idUsuario FROM usuarios " +
                "WHERE nombre ='" + nombre + "'", null)
        if (cursor.moveToNext()) {
            return cursor.getInt(0)
        } else {
            db.execSQL("INSERT INTO usuarios VALUES (null, " +
                    "'" + nombre + "', 'correo@example.com')")
            return buscaInserta(db, nombre)
        }
    }

    override fun listaPuntuaciones(cantidad: Int): ArrayList<String> {
        val result = ArrayList<String>()
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT puntos, nombre FROM " +
                "puntuaciones2, usuarios " +
                "WHERE puntuaciones2.usuario = usuarios.idUsuario " +
                "ORDER BY puntos DESC LIMIT " + cantidad, null)
        while (cursor.moveToNext()) {
            result.add(cursor.getInt(0).toString() + " " + cursor.getString(1))
        }
        cursor.close()
        db.close()
        return result
    }

    override fun onCreate(db: SQLiteDatabase) {
        // Los campos INTEGER PRIMARY KEY son autoincrementados
        // por defecto en SQLite. Para obtener un nuevo valor,
        // insertar un null en dicha columna
        var sql = "CREATE TABLE usuarios (" +
                "idUsuario INTEGER PRIMARY KEY, " +
                "nombre TEXT, correo TEXT)"
        db.execSQL(sql)
        sql = "CREATE TABLE puntuaciones2 (" +
                "idPuntuacion INTEGER PRIMARY KEY, " +
                "puntos INTEGER, fecha LONG, usuario INTEGER, " +
                "FOREIGN KEY (usuario) REFERENCES usuarios (idUsuario))"
        db.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion == 1 && newVersion == 2) {
            //Llamamos a onCreate para crear las nuevas tablas
            onCreate(db)
            // Seleccionamos registros en la tabla antigua
            var cursor = db.rawQuery("SELECT puntos, nombre, fecha " + "FROM puntuaciones", null)
            // Creamos los registros en la nueva tabla
            while (cursor.moveToNext()) {
                guardarPuntuacion(db, cursor.getInt(0), cursor.getString(1), cursor.getInt(2).toLong())
            }
            cursor.close()
            db.execSQL("DROP TABLE puntuaciones")
        }
    }

    override fun hayPuntuaciones(): Boolean {
        val db = readableDatabase
        val cursor = db.rawQuery("SELECT COUNT(*) FROM puntuaciones2",
                null)
        cursor.moveToFirst()
        return cursor.getInt(0) > 0
    }

}