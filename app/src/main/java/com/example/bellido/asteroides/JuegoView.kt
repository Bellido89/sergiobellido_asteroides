package com.example.bellido.asteroides

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.SoundPool
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.example.bellido.asteroides.R.string.salir
import com.example.bellido.asteroides.R.drawable.nave
import java.io.Console


class JuegoView : View, SensorEventListener {

    // //// THREAD Y TIEMPO  //// //
    // THREAD ENCARGADO DE PROCESAR EL JUEGO //
     var thread : ThreadJuego = ThreadJuego()
    // CADA CUANTO QUEREMOS PROCESAR CAMBIOS (ms) //
     val PERIODO_PROCESO : Int = 50
    // CUANDO SE REALIZO EL ULTIMO PROCESO
     var ultimoProceso : Long = 0


    // //// ASTEROIDES //// //
     var Asteroides: ArrayList<Grafico> // Vector con los Asteroides
     var numAsteroides: Int = 5 // Número inicial de asteroides
     var numFragmentos: Int = 3 // Fragmentos en que se divide

    // /// NAVE /// //
    var nave: Grafico
    var giroNave : Int = 0
    var aceleracionNave : Float = 0.0f
    val PASO_GIRO_NAVE = 5
    val PASO_ACELERACION_NAVE = 0.5f

    // /// Manejo Nave pantalla táctil /// //

    var  mx = 0
    var my = 0
    var disparo = false
    val DIFERENCIA_HORIZONTAL = 12
    val DIFERENCIA_VERTICAL = 12
    val DIVISOR_GIRO_NAVE = 2f
    val DIVISOR_ACELERACION_NAVE = 25f


    // /// VARIABLES PARA CONTROLAR LOS SENSORES /// //

     var hayInclinacionInicial = false
     var inclinacionInicial : Float = 0.0f
    var aceleracionInicial : Float = 0.0f

     var mGravedad : FloatArray? = null
     var mGeomagnetismo : FloatArray? = null


    // /// MISIL /// //

    var misil : Misil
    val PASO_VELOCIDA_MISIL : Int = 12
    var misilActivo = true
    var misiles : ArrayList<Misil>

    // DRAWABLES
    var drawableMisil: Drawable


    //SENSORES
    var sensorManager : SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    lateinit var listaSensores:List<Sensor>

    //VARIABLES MEDIA PLAYER
    var soundPool : SoundPool
    var idDisparo : Int = 0
    var idExplosion : Int = 0
    lateinit var mpDisparo : MediaPlayer
    lateinit var mpExplosion : MediaPlayer


    //VARIABLE FRAGMENTANDO ASTEROIDES
    var drawableAsteroides : ArrayList<Drawable> = ArrayList(3)
    lateinit var numAstString : String


    // /// PUNTUACIÓN /////
    private var puntuacion = 0


    // /// ACTIVITY QUE CONTIENE A ESTA VIEW
    private var contenedor: Activity? = null

    fun setContenedor(contenedor: Activity) {
        this.contenedor = contenedor
    }


    var prefs : SharedPreferences
    var sensoresOn : String



    //METODO SALIR
    private fun salir() {
        val bundle = Bundle()
        bundle.putInt("puntuacion", puntuacion)
        val intent = Intent()
        intent.putExtras(bundle)
        contenedor!!.setResult(Activity.RESULT_OK, intent)
        contenedor!!.finish()
    }

    //CONSTRUCTOR

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        var drawableNave: Drawable
        var drawableAsteroide: Drawable
        Asteroides = arrayListOf()
        drawableNave = context!!.resources.getDrawable(R.drawable.nave)
        nave = Grafico(this, drawableNave)
        drawableMisil = context!!.resources.getDrawable(R.drawable.misil1)
        misil = Misil(this, drawableMisil)
        misiles = ArrayList<Misil>()
        soundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        idDisparo = soundPool.load(context, R.raw.disparo,0)
        idExplosion = soundPool.load(context, R.raw.explosion, 0)
        drawableAsteroides.add(context.resources.getDrawable(R.drawable.asteroide1))
        drawableAsteroides.add(context.resources.getDrawable(R.drawable.asteroide2))
        drawableAsteroides.add(context.resources.getDrawable(R.drawable.asteroide3))
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        sensoresOn = prefs.getString("controles", "").toString()

        if (sensoresOn != "2"){
            desactivarSensores()
        } else { activarSensores() }


        for (i in 0..numAsteroides){
            var asteroide = Grafico(this, drawableAsteroides[0])
            asteroide.incY = Math.random() * 4 - 2
            asteroide.incX = Math.random() * 4 - 2
            asteroide.angulo = (Math.random() * 360).toInt()
            asteroide.rotacion = (Math.random() * 8 - 4).toInt()
            Asteroides.add(asteroide)
        }





    }


    override fun onSensorChanged(event: SensorEvent?) {
        synchronized(this) {
            when (event?.sensor?.getType()) {
                Sensor.TYPE_ACCELEROMETER -> mGravedad = event.values
                Sensor.TYPE_MAGNETIC_FIELD -> mGeomagnetismo = event.values
            }
            if (mGravedad != null && mGeomagnetismo != null) {
                val R = FloatArray(9)
                val I = FloatArray(9)
                val exito = SensorManager
                        .getRotationMatrix(R, I,
                                mGravedad, mGeomagnetismo)
                if (exito) {
                    val orientacion = FloatArray(3)
                    // ángulos en radianes
                    SensorManager.getOrientation(R, orientacion)
                    val valor = (-orientacion[1] * 360 / (2 * Math.PI.toFloat()))
                    val valorAcelerar = orientacion[2]
                    if (!hayInclinacionInicial) {
                        inclinacionInicial = valor // Eje Y
                        aceleracionInicial = valorAcelerar
                        hayInclinacionInicial = true
                    }
                    giroNave = ((valor - inclinacionInicial)  / 3).toInt()

                    if (valorAcelerar > aceleracionInicial+0.2){
                        aceleracionNave = +PASO_ACELERACION_NAVE
                    }

                    if (valorAcelerar < aceleracionInicial-0.2){
                        aceleracionNave = -PASO_ACELERACION_NAVE
                    }

                }
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {

        var activado = false

        if (sensoresOn == "1"){
            activado = true
        }
        super.onTouchEvent(event)
        var x = event!!.getX()
        var y = event.getY()

        when (event.action){
            MotionEvent.ACTION_DOWN -> disparo = true
            MotionEvent.ACTION_MOVE -> {
                var dx = Math.abs(x - mx)
                var dy = Math.abs(y - my)
                if (dy < DIFERENCIA_VERTICAL && dx > DIFERENCIA_HORIZONTAL){
                    giroNave = Math.round((x - mx) / DIVISOR_GIRO_NAVE)
                    disparo = false
                } else if (dx < DIFERENCIA_HORIZONTAL && dy > DIFERENCIA_VERTICAL){
                    aceleracionNave = Math.round((my - y) / DIVISOR_ACELERACION_NAVE).toFloat()
                    disparo = false
                }
            }
            MotionEvent.ACTION_UP -> {
                giroNave = 0
                aceleracionNave = 0f
                if(disparo){
                    activaMisil()
                }
            }
        }
        mx = x.toInt(); my = y.toInt()
        return  activado
    }




    override fun onKeyDown(codigoTecla: Int, evento: KeyEvent?): Boolean {


        var procesar : Boolean = false
        if (sensoresOn == "0"){
            procesar = true
        }
        when(codigoTecla){
            KeyEvent.KEYCODE_DPAD_UP -> aceleracionNave = +PASO_ACELERACION_NAVE
            KeyEvent.KEYCODE_DPAD_DOWN -> aceleracionNave = -PASO_ACELERACION_NAVE
            KeyEvent.KEYCODE_DPAD_LEFT -> giroNave = -PASO_GIRO_NAVE
            KeyEvent.KEYCODE_DPAD_RIGHT -> giroNave = +PASO_GIRO_NAVE


            else -> procesar = false
        }
        return procesar
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        var procesar = false
        if (sensoresOn == "0"){
            procesar = true
        }
        when(keyCode){
            KeyEvent.KEYCODE_DPAD_UP -> aceleracionNave = 0.toFloat()
            KeyEvent.KEYCODE_DPAD_DOWN -> aceleracionNave = 0.toFloat()
            KeyEvent.KEYCODE_DPAD_LEFT -> giroNave = 0
            KeyEvent.KEYCODE_DPAD_RIGHT -> giroNave = 0
            KeyEvent.KEYCODE_ENTER -> activaMisil()

        }

        return procesar
    }

    override fun onSizeChanged(ancho: Int, alto: Int, ancho_anter: Int, alto_anter: Int) {
        super.onSizeChanged(ancho, alto, ancho_anter, alto_anter)
        // Una vez que conocemos nuestro ancho y alto.


        nave.posX = (ancho/2).toDouble()
        nave.posY = (alto/2).toDouble()

        //FOR EACH EN KOTLIN
        for(asteroide : Grafico in Asteroides) {
            do
            {
                asteroide.posX=(Math.random() * (ancho - asteroide.ancho))
                asteroide.posY=(Math.random() * (alto - asteroide.alto))
            }
            while (asteroide.distancia(nave) < (ancho + alto) / 5)
        }

        ultimoProceso = System.currentTimeMillis()
        thread.start()


    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for(asteroide: Grafico in Asteroides){
            asteroide.dibujaGrafico(canvas!!)
        }

        if(!misiles!!.isEmpty()){
            for (misil : Misil in misiles!!){
                misil.dibujaGrafico(canvas!!)
            }
        }


        nave.dibujaGrafico(canvas!!)
    }

    //METODO ACTUALIZA FISICA, ENCARGADO DE DAR MOVIMIENTO AL JUEGO
    fun actualizaFisica(){
        var ahora = System.currentTimeMillis()

        if (ultimoProceso + PERIODO_PROCESO > ahora) { return }

        var retardo = (ahora - ultimoProceso) / PERIODO_PROCESO
        ultimoProceso = ahora

        nave.angulo = (nave.angulo + giroNave * retardo).toInt()

        var nIncX = nave.incX + aceleracionNave * Math.cos(Math.toRadians(nave.angulo.toDouble())) * retardo
        var nIncY = nave.incY + aceleracionNave * Math.sin(Math.toRadians(nave.angulo.toDouble())) * retardo

        if (Math.hypot(nIncX, nIncY) <= Grafico().MAX_VELOCIDAD){
            nave.incX = nIncX
            nave.incY = nIncY
        }

        nave.incrementaPos(retardo.toDouble())
        for (asteroide : Grafico in Asteroides){
            asteroide.incrementaPos(retardo.toDouble())
        }


        /*if (misilActivo){
            misil.incrementaPos(retardo.toDouble())
            misil.tiempo -= retardo.toInt()

            if (misil.tiempo < 0){
                misilActivo = false
            } else {

                var contador = 0
                while (contador < Asteroides.size){
                    if (misil.verificaColision((Asteroides.elementAt(contador)))) {
                        destruyeAsteroide(contador)
                    }
                    contador++
                }
            }
        }*/


        if (!misiles!!.isEmpty()){
            var destruccion = false
            var contador = 0
            var contador2 =0
            while (contador < misiles!!.size && !destruccion){
                misil = misiles!!.elementAt(contador)
                misil.incrementaPos(retardo.toDouble())
                misil.tiempo = (misil.tiempo -retardo).toInt()
                if(misil.tiempo < 0){
                    misiles!!.removeAt(contador)
                    destruccion=true
                } else {
                    while (contador2 < Asteroides.size){
                        if (misil.verificaColision(Asteroides.elementAt(contador2))){
                            destruyeAsteroide(contador)
                            misiles!!.removeAt(contador)
                            destruccion = true
                        }
                        contador2++
                    }
                }
                contador++
            }
        }

        for (asteroide in Asteroides) {
            if (asteroide.verificaColision(nave)) {
                salir()
            }
        }


    }

    fun destruyeAsteroide (i : Int){
        var tamanyo : Int
        var prefs = PreferenceManager.getDefaultSharedPreferences(this.context)
        numAstString = prefs.getString("fragmentos", "3" )
        numFragmentos = numAstString.toInt()


        if(Asteroides.get(i).drawable!=drawableAsteroides[2]){
            if (Asteroides.get(i).drawable == drawableAsteroides[1]){
            tamanyo = 2
                puntuacion += 500
            } else {
            tamanyo = 1
                puntuacion += 250
            }

        for (i in 0..numFragmentos){
            var asteroide : Grafico = Grafico(this, drawableAsteroides[tamanyo])
            asteroide.posX = Asteroides.get(i).posX
            asteroide.posY = Asteroides.get(i).posY
            asteroide.incX = Math.random()*7-2-tamanyo
            asteroide.incY = Math.random()*7-2-tamanyo
            asteroide.angulo = (Math.random()*360).toInt()
            asteroide.rotacion = (Math.random()*8-4).toInt()
            Asteroides.add(asteroide);
        }
        }else{
            puntuacion += 1000
        }
        Asteroides.removeAt(i)
        soundPool.play(idExplosion,1f,1f,0,0,1f)
        //mpExplosion.start()
       // misilActivo = false

        if (Asteroides.isEmpty()) {
            salir();
        }
        Log.d("PUNTUACION", puntuacion.toString())
    }

    fun activaMisil(){


        misil = Misil(this, drawableMisil)
        misil.posX = nave.posX + nave.ancho/2 - misil.ancho/2
        misil.posY = nave.posY + nave.alto/2 - misil.alto/2
        misil.angulo = nave.angulo
        misil.incX = Math.cos(Math.toRadians(misil.angulo.toDouble())) * PASO_VELOCIDA_MISIL
        misil.incY = Math.sin(Math.toRadians(misil.angulo.toDouble())) * PASO_VELOCIDA_MISIL
        misil.tiempo = Math.min(this.width / Math.abs(misil.incX), this.height / Math.abs(misil.incY)).toInt() - 2
        misiles!!.add(misil)
        soundPool.play(idDisparo,1f,1f,1,0,1f)
        //mpDisparo.start()
    }


    fun activarSensores(){
        listaSensores = sensorManager
                .getSensorList(Sensor.TYPE_ACCELEROMETER)
        if (!listaSensores.isEmpty())
        {
            val acelerometerSensor = listaSensores.get(0)
            sensorManager.registerListener(this, acelerometerSensor,
                    SensorManager.SENSOR_DELAY_GAME)
        }
        listaSensores = sensorManager
                .getSensorList(Sensor.TYPE_MAGNETIC_FIELD)
        if (!listaSensores.isEmpty())
        {
            val magneticSensor = listaSensores.get(0)
            sensorManager.registerListener(this, magneticSensor,
                    SensorManager.SENSOR_DELAY_GAME)
        }
    }


    fun desactivarSensores(){
        sensorManager.unregisterListener(this)
    }


    //CLASE THREADJUEGO

    inner class ThreadJuego : Thread(){
        var lock = java.lang.Object()
        var pausa : Boolean = false
        var corriendo : Boolean = false

        fun  pausar() {
            pausa = true
        }

        fun reanudar() = synchronized(lock){
            pausa = false
            lock.notify()
        }

        fun detener(){
            corriendo = false
            if (pausa) reanudar()
        }

        override fun run() {
            pausa = false
            corriendo = true

            while(corriendo){
                actualizaFisica()
                synchronized(this){
                    while (pausa){
                        try {
                            lock.wait()
                        } catch (e : Exception){}
                    }
                }
            }
        }
    }
}
