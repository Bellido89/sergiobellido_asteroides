package com.example.bellido.asteroides

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

/**
 * Created by belli on 22/11/2017.
 */
class AdapterRecycler (var puntuaciones : AlmacenPuntuaciones) : RecyclerView.Adapter<AdapterRecycler.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder!!.asignarDatos(puntuaciones.listaPuntuaciones(10).get(position))
    }

    override fun getItemCount(): Int {
        return puntuaciones.listaPuntuaciones(10).size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        var view : View =  LayoutInflater.from(parent!!.context).inflate(R.layout.elemento_lista, null, false)
        return ViewHolder(view)
    }


    class ViewHolder(vista  : View) : RecyclerView.ViewHolder(vista), View.OnClickListener {

        override fun onClick(v: View?) {
            Toast.makeText(v!!.context, "Puntuacion: " + texto.text, Toast.LENGTH_SHORT).show()
        }

        var icono : ImageView = vista.findViewById(R.id.icono)
        var texto : TextView = vista.findViewById(R.id.titulo)
        var fila = vista

        fun asignarDatos (s : String){
            fila.setOnClickListener(this)
            texto.text = s

            when ((Math.round(Math.random() * 3)).toInt()) {
                0 -> icono.setImageResource(R.drawable.asteroide1)
                1 -> icono.setImageResource(R.drawable.asteroide2)
                else -> icono.setImageResource(R.drawable.asteroide3)
            }
        }
    }
} 