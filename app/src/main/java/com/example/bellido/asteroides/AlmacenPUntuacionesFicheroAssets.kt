package com.example.bellido.asteroides

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Bellido on 14/02/2018.
 */
class AlmacenPUntuacionesFicheroAssets(private var context: Context) : AlmacenPuntuaciones {


    companion object {
        private var instancia: AlmacenPUntuacionesFicheroAssets? = null
            private set
    }

    var FICHERO : String = "puntuaciones.txt"

    fun getInstance(): AlmacenPUntuacionesFicheroAssets? {
        if(instancia == null) {
            instancia = AlmacenPUntuacionesFicheroAssets(context)
        }
        return instancia
    }

    override fun guardarPuntuacion(puntos: Int, nombre: String, fecha: Long) {

    }

    override fun listaPuntuaciones(cantidado: Int): ArrayList<String> {

        val result = ArrayList<String>()
        try {
            val f = context.resources.assets.open(FICHERO)
            val entrada = BufferedReader(InputStreamReader(f))
            var n = 0
            var linea: String?
            do {
                linea = entrada.readLine()
                if (linea != null) {
                    result.add(linea)
                    n++
                }
            } while (n < cantidado && linea != null)
            f.close()
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

        return result
    }

    override fun hayPuntuaciones(): Boolean {
        val puntuaciones = listaPuntuaciones(10)
        return !puntuaciones.isEmpty()
    }
}