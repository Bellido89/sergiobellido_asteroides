package com.example.bellido.asteroides

import android.util.Log
import org.xml.sax.InputSource
import org.xml.sax.XMLReader
import java.io.InputStream
import java.util.*
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory
import android.util.Xml
import java.io.OutputStream
import kotlin.collections.ArrayList


/**
 * Created by Bellido on 16/02/2018.
 */
class ListaPuntuacionesSAX {

    var listaPuntuaciones : ArrayList<Puntuacion> = ArrayList<Puntuacion>()

    fun nuevo(puntos : Int, nombre : String, fecha : Long){
        var puntuacion  = Puntuacion()
        puntuacion.puntos = puntos
        puntuacion.nombre = nombre
        puntuacion.fecha = fecha
        listaPuntuaciones.add(puntuacion)
    }

    fun aVectorString() : ArrayList<String>{
        var resultado : ArrayList<String> = ArrayList()
        for (puntuacion  in listaPuntuaciones){
            resultado.add(puntuacion.nombre + " " + puntuacion.puntos)
        }

        return resultado
    }

    @Throws(Exception::class)
    fun leerXML(entrada : InputStream) {
        var factory : SAXParserFactory = SAXParserFactory.newInstance()
        var parser : SAXParser = factory.newSAXParser()
        var reader : XMLReader = parser.xmlReader
        var manejadorXML  = ManejadorXML()
        reader.contentHandler = manejadorXML
        reader.parse(InputSource(entrada))
        listaPuntuaciones = manejadorXML.getListaPuntuaciones()
    }


    fun escribirXML(salida: OutputStream) {
        val serializador = Xml.newSerializer()
        try {
            serializador.setOutput(salida, "UTF-8")
            serializador.startDocument("UTF-8", true)
            serializador.startTag("", "lista_puntuaciones")
            for (puntuacion in listaPuntuaciones) {
                serializador.startTag("", "puntuacion")
                serializador.attribute("", "fecha",
                        puntuacion.fecha.toString())
                serializador.startTag("", "nombre")
                serializador.text(puntuacion.nombre)
                serializador.endTag("", "nombre")
                serializador.startTag("", "puntos")
                serializador.text(puntuacion.puntos.toString())
                serializador.endTag("", "puntos")
                serializador.endTag("", "puntuacion")
            }
            serializador.endTag("", "lista_puntuaciones")
            serializador.endDocument()
        } catch (e: Exception) {
            Log.e("Asteroides", e.message, e)
        }

    }


}